#pragma once

/**
 * @brief A fluffy cat
 * 
 */
struct cat {
/**
 * @brief Make this cat look super cute and nice :)
 * 
 */
  void make_cute(int cuteRang);

/**
 * @brief Make this cat happy!
 * 
 */
  void make_happy();
};