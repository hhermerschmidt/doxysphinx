# DoxySphinx

Generate nice looking 😎 documentation for C/C++ source code 🤓 using Doxygen and Sphinx 😃.

## Workflow
```bash
# Activate virtual environment (to make sphinx available)
source env/bin/activate

# Create build directory
mkdir build

# Initialise cmake
cd build
cmake ..

# Build binaries (and documentation)
cmake --build .

# Copy binaries (and documentation) into destination folder
cmake --install .
```

## Package Content
* CatCutifier/: C++ source code
* docs/: Documentation source code
* cmake/: Cmake auxiliary folder for detecting sphinx executable