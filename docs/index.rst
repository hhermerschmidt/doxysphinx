.. CatCutifier documentation master file, created by
   sphinx-quickstart on Mon Feb  8 15:33:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CatCutifier's documentation!
=======================================

.. doxygenstruct:: cat
   :members:
  
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   GettingStarted



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
